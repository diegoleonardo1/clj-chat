(ns websocket-tutorial.with-aleph
  (:require [compojure.core :as compojure :refer [GET]]
            [ring.middleware.params :as params]
            [compojure.route :as route]
            [aleph.http :as http]
            [byte-streams :as bs]
            [manifold.stream :as s]
            [manifold.deferred :as d]
            [manifold.bus :as bus]
            [clojure.core.async :as a]
            [websocket-tutorial.simple-ws-client :as ws-client]
            [ring.util.response :refer [redirect]]))

(def non-websocket-request
  {:status 400
   :headers {"content-type" "application/text"}
   :body "Expected a websocket request."})

(defn echo-handler
  [req]
  (let [conn @(http/websocket-connection req)]
    (println conn)
    (s/connect conn conn)))

(def chatrooms (bus/event-bus))

(defn former-chat-handler
  [req]
  (d/let-flow [conn (d/catch
                        (http/websocket-connection req)
                        (fn [_] nil))]
              (if-not conn
                ;; if it wasn't a valid websocket handshake, return an error
                non-websocket-request
                ;; otherwise, take the first two messages, which give us the chatroom and name
                (d/let-flow [room (s/take! conn)
                             name (s/take! conn)]
                            ;; take all messages from the chatroom, and feed them to the client
                            (s/connect
                             (bus/subscribe chatrooms room)
                             conn)
                            ;; take all messages from the client, prepend the name, and publish it to the room
                            (s/consume
                             #(bus/publish! chatrooms room %)
                             (->> conn
                                  (s/map #(str name ": " %))
                                  (s/buffer 100)))
                            ;; Compojure expects some sort of HTTP response, so just give it `nil`
                            nil))))

(defn chat-handler
  [req]
  (println (str "request: " req))
  (let [room (get (:query-params req) "room")
        user (get (:query-params req) "user")]
    (d/let-flow [conn (d/catch
                          (http/websocket-connection req)
                          (fn [_] nil))]
                (if-not conn
                  ;; if it wasn't a valid websocket handshake, return an error
                  non-websocket-request
                  ;; otherwise, take the first two messages, which give us the chatroom and name
                  (do (s/connect
                       (bus/subscribe chatrooms room)
                       conn)
                      ;; take all messages from the client, prepend the name, and publish it to the room
                      (s/consume
                       #(bus/publish! chatrooms room %)
                       (->> conn
                            (s/map #(str user ": " %))
                            (s/buffer 100)))
                      ;; Compojure expects some sort of HTTP response, so just give it `nil`
                      nil)))))

(def handler
  (params/wrap-params
   (compojure/routes
    (GET "/echo" [] echo-handler)
    (GET "/chat" [] chat-handler)
    (GET "/former-chat" [] former-chat-handler)
    (GET "/" {c :context} (redirect (str c "/index.html")))
    (route/resources "/"))))

(def state (atom nil))

(defn start []
  (reset! state (http/start-server handler {:port 10000})))

(defn stop []
  (.close @state)
  (reset! state nil))

#_(start)
#_(stop)

(comment
  (def conn1 (ws-client/ws-conn "ws://localhost:10000/chat?room=room1&user=bar"
                                :on-connect (fn [sock]
                                              (println sock)
                                              (println "Connected."))
                                :on-msg (fn [sock msg] (println ">" msg))
                                :on-close (fn [sock desc] (println "Closed:" desc))))

  (def conn2 (ws-client/ws-conn "ws://localhost:10000/chat?room=room1&user=foo"
                                :on-connect (fn [sock]
                                              (println sock)
                                              (println "Connected."))
                                :on-msg (fn [sock msg] (println ">" msg))
                                :on-close (fn [sock desc] (println "Closed:" desc))))

  (ws-client/ws-send conn1 "bar")
  (ws-client/ws-send conn2 "foobar")

  )

(comment
  (def client1 @(aleph.http/websocket-client "ws://localhost:10000/chat"))
  (def client2 @(aleph.http/websocket-client "ws://localhost:10000/chat"))

  (s/put-all! client1 ["shoes and ships" "Alice"])
  (s/put-all! client2 ["shoes and ships" "Bob"])

  (s/put! client1 "hello")

  @(s/take! client1)
  @(s/take! client2)

  (s/put! client2 "hi!")

  @(s/take! client1)
  @(s/take! client2)


  )
