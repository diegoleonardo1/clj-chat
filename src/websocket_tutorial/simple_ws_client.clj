(ns websocket-tutorial.simple-ws-client
  (:require [aleph.http :as http]
            [manifold.stream :as s]
            [manifold.deferred :as d]))

(defn ws-conn
  "Open a WebSocket connection with the given handlers.

  All handlers take [sock msg] except for :on-connect, which only takes [sock]
  -- `sock` being the duplex stream.

  - :on-connect   Called once when the connection is established.
  - :on-msg       Called with each message.
  - :on-close     Called once upon socket close with a map {:stat _, :desc _}.

  The optional :aleph parameter is configuration to pass through to Aleph's
  WebSocket client."
  [uri & {:keys [on-connect on-msg on-close aleph]}]
  (let [sock (http/websocket-client uri aleph)
        handle-messages (fn [sock]
                          (d/chain
                            (s/consume (fn [msg] (on-msg sock msg)) sock)
                            (fn [sock-closed] sock)))
        handle-shutdown (fn [sock]
                          (let [state (:sink (s/description sock))]
                            (on-close
                              sock {:stat (:websocket-close-code state)
                                    :desc (:websocket-close-msg state)})))]
    (d/chain sock #(doto % on-connect) handle-messages handle-shutdown)
    @sock))

(defn ws-send [sock msg] (s/put! sock msg))
(defn ws-close [sock] (s/close! sock))
