(ns websocket-tutorial.simple-ws-example
  (:require [aleph.http :as http]
            [clojure.string :as string]
            [manifold.stream :as s]
            [manifold.deferred :as d]
            [websocket-tutorial.simple-ws-client :as ws.client]
            [compojure.core :as compojure :refer [GET defroutes]]
            [compojure.route :as route]
            [ring.middleware.params :as params]
            [ring.util.response :refer [redirect]]))

(defn uppercase-handler
  "Handle a message by upper casing it and echoing it back."
  [socket msg]
  (s/put! socket (string/upper-case msg)))

(defn chat-handler
  [req]
  (d/chain
   (http/websocket-connection req)
   (fn [socket]
     (s/consume (fn [msg] (uppercase-handler socket msg)) socket))))

(defroutes routes
  (GET "/chat" [] chat-handler)
  (GET "/" {c :context} (redirect (str c "/index.html")))
  (route/resources "/"))

(def state (atom nil))

(defn start []
  (reset! state (http/start-server
                 routes
                 {:port 9999})))

(defn stop []
  (.close @state)
  (reset! state nil))

#_(start)
#_(stop)

(comment
  (def client
    (ws.client/ws-conn "ws://127.0.0.1:9999/chat"
                       :on-connect (fn [sock] (println "Connected."))
                       :on-msg (fn [sock msg] (println ">" msg))
                       :on-close (fn [sock desc] (println "Closed:" desc))))

  (ws.client/ws-send client "A message")
  (ws.client/ws-send client "Another message")


)
